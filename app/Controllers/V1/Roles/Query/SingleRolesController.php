<?php

namespace App\Controllers\V1\Roles\Query;
use App\Controllers\BaseController;
use App\Models\RolesModel;
use CodeIgniter\API\ResponseTrait;

class SingleRolesController extends BaseController
{
    use ResponseTrait;
    public function index($id)
    {
        $roles = new RolesModel();
        $data = $roles->where('roles_id', $id)->first();
        return $this->setResponseFormat('json')->respond(['data' => $data]);
    }

}
