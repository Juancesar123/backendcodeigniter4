<?php

namespace App\Controllers\V1\Auth\Command;
use App\Controllers\BaseController;
use App\Models\UsersModel;
use Exception;
use Firebase\JWT\JWT;

class LoginController extends BaseController
{

    public function index()
    {
        try {
            
            $rules = setting('Validation.login') ?? [
                'email' => config('Auth')->emailValidationRules,
                'password' => [
                    'label' => 'Auth.password',
                    'rules' => 'required',
                ],
            ];

            if (! $this->validateData((array)$this->request->getVar(), $rules, [], config('Auth')->DBGroup)) {
                return $this->response
                    ->setJSON(['errors' => $this->validator->getErrors()])
                    ->setStatusCode(401);
            }

            // Get the credentials for login
            $credentials             = $this->request->getVar(setting('Auth.validFields'));
            $credentials             = array_filter($credentials);
            $credentials['password'] = $this->request->getVar('password');
            // Attempt to login
            $result = auth()->attempt($credentials);
            if (! $result->isOK()) {
                return $this->response
                    ->setJSON(['error' => $result->reason()])
                    ->setStatusCode(401);
            }
    
            // Generate token and return to client
            $token = auth()->user()->generateAccessToken(getenv('JWT_SECRET'));
            $user = auth()->user();
            return $this->response
                ->setJSON(['code'=> 200,'data' => [ 'token' => $token->raw_token,'users' => $user]]);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
}
