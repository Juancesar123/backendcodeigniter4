<?php

namespace App\Controllers\V1\Roles\Query;
use App\Controllers\BaseController;
use App\Models\RolesModel;
use CodeIgniter\API\ResponseTrait;

class ListRolesController extends BaseController
{
    function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorized");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    }
    public function index()
    {
        $combobox = $this->request->getGet('is_combobox');
        $roles = new RolesModel();
        $perPage    = (int) ($this->request->getGet('per_page') ?? 5);
        
        if(!$combobox){

            $data = [
                'data' => $roles->paginate($perPage),
                'pagination' => [
                    'total_pages' => $roles->pager->getPageCount(),
                    'current_pages' => $roles->pager->getCurrentPage(),
                    'total_data' => $roles->countAllResults(),
                    'first_page' => $roles->pager->getFirstPage(),
                    'previous' => $roles->pager->getPreviousPageURI(),
                    'next' =>  $roles->pager->getNextPageURI(),
                    'last_page' => $roles->pager->getLastPage(),
                ]
            ];
            return $this->setResponseFormat('json')->respond($data);
        }
        $data = [
            'data' => $roles->findAll(),
        ];
        return $this->setResponseFormat('json')->respond($data);
    }

}
