<?php

namespace App\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class AuthFilter implements FilterInterface
{
    /**
     * Do whatever processing this filter needs to do.
     * By default it should not return anything during
     * normal execution. However, when an abnormal state
     * is found, it should return an instance of
     * CodeIgniter\HTTP\Response. If it does, script
     * execution will end and that Response will be
     * sent back to the client, allowing for error pages,
     * redirects, etc.
     *
     * @param RequestInterface $request
     * @param array|null       $arguments
     *
     * @return RequestInterface|ResponseInterface|string|void
     */
    public function before(RequestInterface $request, $arguments = null)
    {
        helper("auth");
        if(!auth("tokens")->loggedIn()){
            $data = [
                'code' => 401,
                'status' => 'UNAUTHORIZED',
                'errors' => [
                    'message' => 'UNAUTHORIZED',
                    'trace' => ''
                ]
            ];
            
            // Ubah data menjadi format JSON
            $response = service('response');
            $response->setJson($data);
            $response->setStatusCode(401);
            return $response;
        }
        $users = auth()->user();
        $db      = \Config\Database::connect();
        $builder = $db->table('user_scope');
        $builder->select('user_scope.*, roles.code');
        $builder->join('roles', 'roles.roles_id = user_scope.role_id', 'left');
        $builder->where('role_id', $users->role_id);
        $usersScope = json_decode($builder->get()->getRow()->scope, true);
        $method = $request->getMethod();
        $path = $request->getUri()->getPath();
        $filtered = array_filter($usersScope, function($item) use ($method, $path) {
            return $item['method'] == $method && $item['path'] == $path;
        });
        if (empty($filtered)) {
            $data = [
                'code' => 403,
                'status' => 'FORBIDDEN',
                'errors' => [
                    'message' => 'FORBIDDEN',
                    'trace' => ''
                ]
            ];
        
            // Ubah data menjadi format JSON
            $response = service('response');
            $response->setJson($data);
            $response->setStatusCode(403);
            return $response;
        }
    }

    /**
     * Allows After filters to inspect and modify the response
     * object as needed. This method does not allow any way
     * to stop execution of other after filters, short of
     * throwing an Exception or Error.
     *
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array|null        $arguments
     *
     * @return ResponseInterface|void
     */
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        //
    }
}
