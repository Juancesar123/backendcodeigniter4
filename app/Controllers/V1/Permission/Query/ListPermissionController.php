<?php

namespace App\Controllers\V1\Permission\Query;
use App\Controllers\BaseController;
use App\Models\PermissionModel;
use App\Models\RolesModel;
use CodeIgniter\API\ResponseTrait;

class ListPermissionController extends BaseController
{
    use ResponseTrait;
    public function index()
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('user_scope');
        $builder->select('user_scope.*, roles.code');
        $builder->join('roles', 'roles.roles_id = user_scope.role_id', 'left');
        
        $page    = (int) ($this->request->getGet('page') ?? 1);
        $perPage    = (int) ($this->request->getGet('per_page') ?? 5);
        $offset = ($page - 1) * $perPage;
        $data = [
            'data' => $builder->get($perPage,$offset)->getResult(),
            'pagination' => [
                'current_pages' => $page,
                'total_data' => $builder->countAllResults(),
            ]
        ];
        return $this->setResponseFormat('json')->respond($data);
    }

}
