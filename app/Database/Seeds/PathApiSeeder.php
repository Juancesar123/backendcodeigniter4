<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class PathApiSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'api_group_name' => 'Roles',
                'path'    => '/v1/role',
                'method'  => 'POST',
                'labels' => 'Create Roles'
            ],
            [
                'api_group_name' => 'Roles',
                'path'    => '/v1/role/{id}',
                'method'  => 'PUT',
                'labels' => 'Update Role'
            ],
            [
                'api_group_name' => 'Roles',
                'path'    => '/v1/role/{id}',
                'method'  => 'DELETE',
                'labels' => 'Delete Role'
            ],
            [
                'api_group_name' => 'Roles',
                'path'    => '/v1/roles',
                'method'  => 'GET',
                'labels' => 'List All Roles'
            ],
            [
                'api_group_name' => 'Users',
                'path'    => '/v1/users',
                'method'  => 'GET',
                'labels' => 'List All Users'
            ],
            [
                'api_group_name' => 'Users',
                'path'    => '/v1/user',
                'method'  => 'POST',
                'labels' => 'Create Users'
            ],
            [
                'api_group_name' => 'Users',
                'path'    => '/v1/user/{id}',
                'method'  => 'PUT',
                'labels' => 'Update Users'
            ],
            [
                'api_group_name' => 'Users',
                'path'    => '/v1/user/{id}',
                'method'  => 'DELETE',
                'labels' => 'Delete Users'
            ],
        ];
        // Using Query Builder
        $this->db->table('path_api')->insertBatch($data);
    }
}
