<?php

namespace App\Controllers\V1\Permission\Query;
use App\Controllers\BaseController;
use App\Models\PermissionModel;
use App\Models\RolesModel;
use CodeIgniter\API\ResponseTrait;

class SinglePermissionController extends BaseController
{
    use ResponseTrait;
    public function index($id)
    {
        $roles = new PermissionModel();
        $data = $roles->where('scope_id', $id)->first();
        return $this->setResponseFormat('json')->respond(['data' => $data]);
    }

}
