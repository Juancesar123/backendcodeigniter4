<?php

namespace App\Controllers\V1\Roles\Command;
use App\Controllers\BaseController;
use App\Models\RolesModel;
use CodeIgniter\API\ResponseTrait;
use Exception;

class CreateRolesController extends BaseController
{
    use ResponseTrait;
    public function index()
    {
        try {
            $validation = $this->validate([
                'code' => [
                    'rules'  => 'required|is_unique[roles.code]',
                    'errors' => [
                        'is_unique' => 'Role Code is Unique',
                        'required' => 'Role Code Is Required.'
                    ]
                ],
                'roles_description'    => [
                    'rules'  => 'required',
                    'errors' => [
                        'required' => 'Roles Description Is Required.'
                    ]
                ],
            ]);
            if (!$validation) {
                return $this->failValidationErrors($this->validator->getErrors(),'BAD REQUEST','ERROR VALIDATION');
            }
            $request = Request();
            $rolesmodel = new RolesModel();
            $json = $request->getJSON();
            $rolesmodel->insert($json);
        return $this->respondCreated($json);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
}
