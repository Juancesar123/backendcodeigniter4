<?php

namespace App\Controllers\V1\Permission\Command;
use App\Controllers\BaseController;
use App\Models\PermissionModel;
use App\Models\RolesModel;
use CodeIgniter\API\ResponseTrait;
use Exception;

class UpdatePermissionController extends BaseController
{
    use ResponseTrait;
    public function index($id)
    {
        try {
            
            $request = Request();
            $rolesmodel = new PermissionModel();
            $json = $request->getJSON();
            $datascope = [];
            foreach ($json->scope as $key => $value) {
                $datascope [] = [
                    'method' => $value->method,
                    'path' => $value->path
                ];
            };
            $json->scope = json_encode($datascope);
            $rolesmodel->update($id, $json);
            return $this->respondUpdated();
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }  
    }
}
