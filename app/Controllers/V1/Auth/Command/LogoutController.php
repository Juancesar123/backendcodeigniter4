<?php

namespace App\Controllers\V1\Auth\Command;
use App\Controllers\BaseController;
use App\Models\UsersModel;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class LogoutController extends BaseController
{

    public function index()
    {
        auth()->logout();
        return $this->respond(['code' => 200, 'data' => 'sukses'], 200);
    }
}
