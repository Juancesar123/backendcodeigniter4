<?php

namespace App\Controllers\V1\Users\Command;
use App\Controllers\BaseController;
use App\Models\PermissionModel;
use App\Models\RolesModel;
use App\Models\UsersModel;
use CodeIgniter\API\ResponseTrait;

class DeleteUsersController extends BaseController
{
    public function index($id)
    {
        
        $rolesmodel = new UsersModel();
        $rolesmodel->delete($id);
        return $this->respondDeleted([
            "status" => 201,
            "message" => "data deleted success",
            "data" => null
        ]);
    }
}
