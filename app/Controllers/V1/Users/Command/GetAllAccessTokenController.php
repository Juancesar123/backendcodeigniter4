<?php

namespace App\Controllers\V1\Users\Command;
use App\Controllers\BaseController;
use App\Models\UsersModel;
use Exception;
use Firebase\JWT\JWT;

class GetAllAccessTokenController extends BaseController
{

    public function index()
    {
        try {
            $user = auth()->user();
            $tokens = $user->accessTokens();
            return $this->response
                ->setJSON(['code'=> 200,'data' => $tokens]);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
}
