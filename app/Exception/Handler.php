<?php namespace App\Exceptions;

use CodeIgniter\CodeIgniter\Exceptions\ExceptionRenderer;
use CodeIgniter\HTTP\Exceptions\HTTPException;
use CodeIgniter\HTTP\Response;
use CodeIgniter\Validation\Exceptions\ValidationException;
use Exception;
use TheSeer\Tokenizer\TokenCollectionException;

class CustomExceptionRenderer extends CustomExceptionRenderer
{
    public function render($exception, bool $debug): Response
    {
        if ($exception instanceof ValidationException) {
            // Handle ValidationException
            // Return appropriate response
        } elseif ($exception instanceof HTTPException) {
            // Handle HttpException
            // Return appropriate response
        } elseif ($exception instanceof TokenCollectionException) {
            // Handle TokenExpiredException
            // Return appropriate response
        } elseif ($exception instanceof TokenCollectionException) {
            // Handle TokenInvalidException
            // Return appropriate response
        } else {
            // Handle other exceptions
            // Return generic error response
        }
        
        return parent::render($exception, $debug);
    }
}