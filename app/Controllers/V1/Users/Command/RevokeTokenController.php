<?php

namespace App\Controllers\V1\Users\Command;
use App\Controllers\BaseController;
use Exception;

class RevokeTokenController extends BaseController
{

    public function index()
    {
        try {
            $secret = $this->request->getVar('tokens');
            $user = auth()->user();
            $user->revokeAccessTokenBySecret($secret);
            return $this->response
                ->setJSON(['code'=> 200,'data' => [ 'message' => 'Token Successfully Revoke']]);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
}
