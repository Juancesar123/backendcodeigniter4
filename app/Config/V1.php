<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->group('',['filter' => 'options'], function($routes){

    $routes->options('v1/(:any)', static function () {});
    $routes->group('/v1', ['namespace' => 'App\Controllers\V1'], function($routes) {
        //login group
 
        $routes->group('auth', function($routes) {
            $routes->get('logout', 'Auth\Command\LogoutController');
            $routes->post('login', 'Auth\Command\LoginController');
            $routes->post('register', 'Auth\Command\RegisterController');
            $routes->post('refresh', 'Auth\Command\RefreshTokenController');
        });
    
        $routes->get('/', 'Login\Command\LoginController::index');
        //roles crud
        $routes->get('roles','Roles\Query\ListRolesController',['filter' => ['authFilter']]);
        $routes->group('role',['filter' => 'authFilter'] ,function($routes) {
            $routes->post('/', 'Roles\Command\CreateRolesController');
            $routes->put('(:num)', 'Roles\Command\UpdateRolesController::/$1');
            $routes->delete('(:num)', 'Roles\Command\DeleteRolesController::/$1');
            $routes->get('(:num)', 'Roles\Query\SingleRolesController::/$1');
        });
        //permission
        $routes->get('permissions', 'Permission\Query\ListPermissionController',['filter' => ['options','authFilter']]);
        $routes->group('permission', ['filter' => ['options','authFilter']],function($routes) {
            $routes->post('/', 'Permission\Command\CreatePermissionController');
            $routes->put('(:num)', 'Permission\Command\UpdatePermissionController::/$1');
            $routes->delete('(:num)', 'Permission\Command\DeletePermissionController::/$1');
            $routes->get('(:num)', 'Permission\Query\SinglePermissionController::/$1');
        });
        //users
        $routes->get('users', 'Users\Query\ListUsersController',['filter' => ['options','authFilter']]);
        $routes->group('user',['filter' => ['options','authFilter']], function($routes) {
            $routes->get('tokens', 'Users\Command\GetAllAccessTokenController',['filter' => ['options','authFilter']]);
            $routes->get('revoke-tokens', 'Users\Command\RevokeTokenController');
            $routes->post('/', 'Users\Command\CreateUsersController');
            $routes->put('(:num)', 'Users\Command\UpdateUsersController::/$1');
            $routes->delete('(:num)', 'Users\Command\DeleteUsersController::/$1');
            $routes->get('(:num)', 'Users\Query\SingleUsersController::/$1');
        });
        //path api
        $routes->get('path_apis','PathApi\Query\ListPathApiController',['filter' =>['options','authFilter']]);
    });
});
