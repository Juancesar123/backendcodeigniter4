<?php

namespace App\Controllers\V1\Users\Query;
use App\Controllers\BaseController;
use App\Models\PermissionModel;
use App\Models\RolesModel;
use App\Models\UsersModel;
use CodeIgniter\API\ResponseTrait;

class SingleUsersController extends BaseController
{
    public function index($id)
    {
        $roles = new UsersModel();
        $data = $roles->where('users_id', $id)->first();
        return $this->setResponseFormat('json')->respond(['data' => $data]);
    }

}
