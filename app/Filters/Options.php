<?php 
declare(strict_types=1);
namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class Options implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        /** @var ResponseInterface $response */
        $response = service('response');

        // Set your Origin.
        $response->setHeader('Access-Control-Allow-Origin', '*');

        // Set this header if the client sends Cookies.
        $response->setHeader('Access-Control-Allow-Credentials', 'true');
        if ($request->is('OPTIONS')) {
            $response->setStatusCode(204);

            // Set headers to allow.
            $response->setHeader(
                'Access-Control-Allow-Headers',
                '*'
            );

            // Set methods to allow.
            $response->setHeader(
                'Access-Control-Allow-Methods',
                'GET, POST, OPTIONS, PUT, PATCH, DELETE'
            );

            // Set how many seconds the results of a preflight request can be cached.
            $response->setHeader('Access-Control-Max-Age', '3600');
            return $response;
        }
    }

    /**
     * Allows After filters to inspect and modify the response
     * object as needed. This method does not allow any way
     * to stop execution of other after filters, short of
     * throwing an Exception or Error.
     *
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array|null        $arguments
     *
     * @return ResponseInterface|void
     */
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Set your Origin.
        $response->setHeader('Access-Control-Allow-Origin', '*');

        // Set this header if the client sends Cookies.
        $response->setHeader('Access-Control-Allow-Credentials', 'true');

        // Handle 401 or 403 errors
        $statusCode = $response->getStatusCode();
        if ($statusCode === 401 || $statusCode === 403) {
            $response->setHeader('Access-Control-Allow-Headers', '*');
            $response->setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
            $response->setHeader('Access-Control-Max-Age', '3600');
        }

        return $response;
    }
}