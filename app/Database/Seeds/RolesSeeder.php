<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class RolesSeeder extends Seeder
{
    public function run()
    {
        $data = [
            'code' => 'ROL_ADMINISTRATOR',
            'roles_description'    => 'Administrator',
        ];
        // Using Query Builder
        $this->db->table('roles')->insert($data);
    }
}
