<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Scope extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'scope_id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'scope' => [
                'type'       => 'TEXT',
            ],
            'role_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
            ],
        ]);
        // $this->forge->addForeignKey('role_id');
        $this->forge->addKey('scope_id', true);
        $this->forge->createTable('user_scope');
    }

    public function down()
    {
        $this->forge->dropTable('user_scope');
    }
}
