<?php

namespace App\Controllers\V1\Permission\Command;
use App\Controllers\BaseController;
use App\Models\PermissionModel;
use App\Models\RolesModel;
use CodeIgniter\API\ResponseTrait;

class DeletePermissionController extends BaseController
{
    use ResponseTrait;
    public function index($id)
    {
        
        $rolesmodel = new PermissionModel();
        $rolesmodel->delete($id);
        return $this->respondDeleted();
    }
}
