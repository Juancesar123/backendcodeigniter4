<?php

namespace App\Models;

use CodeIgniter\Model;

class PermissionModel extends Model
{
    protected $table      = 'user_scope';
    protected $primaryKey = 'scope_id';
    protected $allowedFields = ['role_id', 'scope'];
}