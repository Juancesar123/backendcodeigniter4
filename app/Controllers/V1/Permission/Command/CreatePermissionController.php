<?php

namespace App\Controllers\V1\Permission\Command;
use App\Controllers\BaseController;
use App\Models\PermissionModel;
use App\Models\RolesModel;
use CodeIgniter\API\ResponseTrait;
use Exception;

class CreatePermissionController extends BaseController
{
    use ResponseTrait;
    public function index()
    {
        try {
            $request = Request();
            $rolesmodel = new PermissionModel();
            $json = $request->getJSON();
            $rolesmodel->insert($json);
            return $this->respondCreated($json,'Success Created');
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
}
