<?php

namespace App\Controllers\V1\Users\Command;
use App\Controllers\BaseController;
use App\Models\PermissionModel;
use App\Models\RolesModel;
use App\Models\UsersModel;
use CodeIgniter\API\ResponseTrait;
use Exception;

class CreateUsersController extends BaseController
{
    public function index()
    {
        try {
            //code...
            
            $validation = $this->validate([
                'username' => [
                    'rules'  => 'required',
                    'errors' => [
                        'required' => 'Username Is Required.'
                    ]
                ],
                'password'    => [
                    'rules'  => 'required',
                    'errors' => [
                        'required' => 'Password Is Required.'
                    ],
                ],
                'role_id'    => [
                    'rules'  => 'required',
                    'errors' => [
                        'required' => 'Roles Is Required.'
                    ],
                ],
            ]);
            if (!$validation) {
                return $this->failValidationErrors($this->validator->getErrors(),'BAD REQUEST','ERROR VALIDATION');
            }
            $request = Request();
            $rolesmodel = new UsersModel();
            $json = $request->getJSON();
            $json->password = password_hash($json->password, PASSWORD_DEFAULT);
            $rolesmodel->insert($json);
            return $this->respondCreated([
                "code" => 201,
                "message" => "data created success",
                "data" => $json
            ]);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
}
