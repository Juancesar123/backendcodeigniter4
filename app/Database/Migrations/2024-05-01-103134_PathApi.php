<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PathApi extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'api_path_id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'api_group_name' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'path' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'method' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'labels' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'created_at' => [
                'type' => 'TIMESTAMP',
                'null' => true
            ],
            'updated_at' => [
                'type' => 'TIMESTAMP',
                'null' => true
            ],
            'deleted_at' => [
                'type' => 'TIMESTAMP',
                'null' => true
            ],
        ]);
        $this->forge->addKey('api_path_id', true);
        $this->forge->createTable('path_api');
    }

    public function down()
    {
        $this->forge->dropTable('path_api');
    }
}
