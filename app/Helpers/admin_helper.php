<?php
$dotenv = Dotenv\Dotenv::createImmutable(APPPATH .'../');
$dotenv->load();
// kumpulan link services
function linkservice($services)
{
    switch (strtoupper($services)) {
        case 'APP_URL':
            $return = getenv('APP_URL');
            break;
        case 'jwt_ttl':
            $return = getenv('JWT_EXP');
            break;
        case 'jwt_refresh_ttl':
            $return = getenv('JWT_REFRESH_TTL');
            break;
        case 'jwt_secret':
            $return = getenv('JWT_SECRET');
            break;
        default:
            $return = '';
    }


    // $BACKEND   = ('BACKEND' == strtoupper($services)) ? $return = 'http://backend.ibid-adms.com/' : '';
    // $ACCOUNT = ('ACCOUNT' == strtoupper($services)) ? $return = 'http://account.ibid-adms.com/' : '';
    // $NOTIF     = ('NOTIF' == strtoupper($services)) ? $return = 'https://charlie.ibid.astra.co.id/backend/service/notif/' : '';
    // $TAKSASI   =  ('TAKSASI' == strtoupper($services)) ? $return = 'https://charlie.ibid.astra.co.id/backend/service/taksasi/' : '';
    // $FRONTEND   = ('FRONTEND' == strtoupper($services)) ? $return = 'https://charlie.ibid.astra.co.id/' : '';
    // $HANDOVER   = ('HANDOVER' == strtoupper($services)) ? $return = 'https://charlie.ibid.astra.co.id/backend/service/handover/' : '';
    // $STOCK     = ('STOCK' == strtoupper($services)) ? $return = 'http://stock.ibid-adms.com/' : '';
    // $MASTER   = ('MASTER' == strtoupper($services)) ? $return = 'http://master.ibid-adms.com/' : '';
    // $FINANCE   = ('FINANCE' === strtoupper($services)) ? $return = 'http://finance.ibid-adms.com/' : '';
    // $NPL      = ('NPL' === strtoupper($services)) ? $return = 'http://npl.ibid-adms.com/' : '';
    // $CMS    = ('CMS' === strtoupper($services)) ? $return = 'https://charlie.ibid.astra.co.id/backend/dapur/' : '';

    // $AMSSCHEDULE    = ('AMSSCHEDULE' === strtoupper($services)) ? $return = 'http://schedule.ibid-ams.com/api/' : '';
    // $AMSSTOCK    = ('AMSSTOCK' === strtoupper($services)) ? $return = 'https://charlie.ibid.astra.co.id/backend/serviceams/stock/api/' : '';
    // $AMSAUTOBID    = ('AMSAUTOBID' === strtoupper($services)) ? $return = 'https://charlie.ibid.astra.co.id/backend/serviceams/autobid/api/' : '';
    // $AMSLOT    = ('AMSLOT' === strtoupper($services)) ? $return = 'https://charlie.ibid.astra.co.id/backend/serviceams/lot/api/' : '';
    // $IMG_PATH = ('IMGS' === strtoupper($services)) ? $return = 'http://img.ibid.astra.co.id/uploads/upload360/' : '';
    // $AMSLOT2    = ('AMSLOT2' === strtoupper($services)) ? $return = 'https://charlie.ibid.astra.co.id/backend/serviceams/lot/' : '';

    return $return;
}

function getPlatform()
{
    return 'adms';
}
function admsapi($statusHeader,$status , $message, $data )
{
	$ci =&get_instance();
	$ci->output->set_header('Access-Control-Allow-Origin: *');
	$ci->output->set_header('Access-Control-Allow-Methods: POST, GET, OPTIONS, OPTION'); 
	$ci->output->set_header('Access-Control-Allow-Headers: Origin');
	$ci->output->set_content_type('application/json');
	$ci->output->set_status_header($statusHeader);
	$ci->output->set_output(json_encode(array ('status' => $status , 'message' =>  $message , 'data' => $data)));
	$ci->output->_display();
	exit();
}
