<?php

namespace App\Controllers\V1\Auth\Command;
use App\Controllers\BaseController;
use App\Models\UserModel;
use App\Models\UsersModel;
use CodeIgniter\Shield\Entities\User;
use Exception;

class RegisterController extends BaseController
{
    public function index()
    {
        try {
            //code...
            
            $validation = $this->validate([
                'username' => [
                    'rules'  => [
                        'required',
                        'max_length[30]',
                        'min_length[3]',
                        'regex_match[/\A[a-zA-Z0-9\.]+\z/]',
                        'is_unique[users.username]',
                    ],
                    'errors' => [
                        'required' => 'Username Is Required.',
                    ]
                ],
                'email' => [
                    'label' => 'Auth.email',
                    'rules' => [
                        'required',
                        'max_length[254]',
                        'valid_email',
                        'is_unique[auth_identities.secret]',
                    ],
                ],
                'password'    => [
                    'rules'  => 'required',
                    'errors' => [
                        'required' => 'Password Is Required.'
                    ]
                ],
            ]);
            if (!$validation) {
                return $this->failValidationErrors($this->validator->getErrors(),'BAD REQUEST','ERROR VALIDATION');
            }
                // Get the User Provider (UserModel by default)
            $users = new UserModel();

            $user = new User([
                'username' => $this->request->getVar('username'),
                'email'    => $this->request->getVar('email'),
                'password' => $this->request->getVar('password'),
                'first_name' =>  $this->request->getVar('first_name'),
                'last_name' =>  $this->request->getVar('last_name'),
                'role_id' =>  $this->request->getVar('role_id'),
                'active' =>  $this->request->getVar('active'),
            ]);
            $users->save($user);

            // To get the complete user object with ID, we need to get from the database
            $user = $users->findById($users->getInsertID());

            // Add to default group
            $users->addToDefaultGroup($user);
            return $this->respondCreated([
                "code" => 201,
                "message" => "data created success",
                "data" => 'wakwaw'
            ]);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
}
