<?php

namespace App\Controllers\V1\Roles\Command;
use App\Controllers\BaseController;
use App\Models\RolesModel;
use CodeIgniter\API\ResponseTrait;
use Exception;

class DeleteRolesController extends BaseController
{
    use ResponseTrait;
    public function index($id)
    {
        try {
            $request = Request();
            $rolesmodel = new RolesModel();
            $json = $request->getJSON();
            $rolesmodel->delete($id);
            return $this->respondDeleted($json,'success deleted');
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
}
