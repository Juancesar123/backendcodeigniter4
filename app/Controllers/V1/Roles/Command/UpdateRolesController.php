<?php

namespace App\Controllers\V1\Roles\Command;
use App\Controllers\BaseController;
use App\Models\RolesModel;
use CodeIgniter\API\ResponseTrait;
use Exception;

class UpdateRolesController extends BaseController
{
    use ResponseTrait;
    public function index($id)
    {
        try {
            $request = Request();
            $rolesmodel = new RolesModel();
            $json = $request->getJSON();
            $rolesmodel->update($id, $json);
            return $this->respondNoContent();  
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
}
