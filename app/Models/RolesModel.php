<?php

namespace App\Models;

use CodeIgniter\Model;

class RolesModel extends Model
{
    protected $table      = 'roles';
    protected $primaryKey = 'roles_id';
    protected $allowedFields = ['code', 'roles_description'];
}