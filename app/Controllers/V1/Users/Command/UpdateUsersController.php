<?php

namespace App\Controllers\V1\Users\Command;
use App\Controllers\BaseController;
use App\Models\PermissionModel;
use App\Models\RolesModel;
use App\Models\UsersModel;
use CodeIgniter\API\ResponseTrait;

class UpdateUsersController extends BaseController
{
    public function index($id)
    {
        $request = Request();
        $rolesmodel = new UsersModel();
        $json = $request->getJSON();
        $json->password = password_hash($json->password, PASSWORD_DEFAULT);
        $rolesmodel->update($id, $json);
        return $this->respondUpdated([
            "status" => 200,
            "message" => "data updated success",
            "data" => $json
        ]);
    }
}
