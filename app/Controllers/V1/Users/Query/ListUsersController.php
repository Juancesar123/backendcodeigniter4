<?php

namespace App\Controllers\V1\Users\Query;
use App\Controllers\BaseController;
use App\Models\PermissionModel;
use App\Models\RolesModel;
use App\Models\UserModel;
use App\Models\UsersModel;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\HTTP\Exceptions\HTTPException;

class ListUsersController extends BaseController
{
    public function index()
    {
        try {
            $userModel = new UserModel();
            $perPage    = (int) ($this->request->getGet('per_page') ?? 5);
            $data = [
                'data' => $userModel->paginate($perPage),
                'pagination' => [
                    'total_pages' => $userModel->pager->getPageCount(),
                    'current_pages' => $userModel->pager->getCurrentPage(),
                    'total_data' => $userModel->countAllResults(),
                    'first_page' => $userModel->pager->getFirstPage(),
                    'previous' => $userModel->pager->getPreviousPageURI(),
                    'next' =>  $userModel->pager->getNextPageURI(),
                    'last_page' => $userModel->pager->getLastPage(),
                ]
            ];
            return $this->setResponseFormat('json')->respond($data);
        } catch (\Exception $e) {
            
            throw new \Exception($e->getMessage(), $e->getCode()); 
        }
       
    }

}
