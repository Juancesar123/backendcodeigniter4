<?php

namespace App\Controllers\V1\PathApi\Query;
use App\Controllers\BaseController;
use App\Models\PathApi;

class ListPathApiController extends BaseController
{
    public function index()
    {
        $pathApi = new PathApi();
        $is_combobox = $this->request->getGet('is_combobox');
        if(!$is_combobox){
            $perPage    = (int) ($this->request->getGet('per_page') ?? 5);
            $data = [
                'data' => $pathApi->paginate($perPage),
                'pagination' => [
                    'total_pages' => $pathApi->pager->getPageCount(),
                    'current_pages' => $pathApi->pager->getCurrentPage(),
                    'total_data' => $pathApi->countAllResults(),
                    'first_page' => $pathApi->pager->getFirstPage(),
                    'previous' => $pathApi->pager->getPreviousPageURI(),
                    'next' =>  $pathApi->pager->getNextPageURI(),
                    'last_page' => $pathApi->pager->getLastPage(),
                ]
            ];
            return $this->setResponseFormat('json')->respond($data);
        }
        $data = [
            'data' => $pathApi->findAll(),
        ];
        return $this->setResponseFormat('json')->respond($data);
    }

}
