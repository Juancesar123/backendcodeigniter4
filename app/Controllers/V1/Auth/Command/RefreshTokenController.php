<?php

namespace App\Controllers\V1\Auth\Command;
use App\Controllers\BaseController;
use App\Models\UsersModel;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class RefreshTokenController extends BaseController
{

    public function index()
    {
        $key = getenv('JWT_SECRET');
        $iat = time(); // current timestamp value
        $exp = $iat + getenv('JWT_EXP');
        $decoded = JWT::decode($this->request->getVar('refresh_token'), new Key($key, 'HS256'));
       
        $userModel = new UsersModel();
        $user = $userModel->where('users_id',$decoded->sub)->first();
        $payload = array(
            "iss" => base_url(),
            "sub" => $user['users_id'],
            "iat" => $iat, //Time the JWT issued at
            "exp" => $exp, // Expiration time of token
            "user" =>[
               'full_name' => $user['first_name'] .' '. $user['last_name'],
               'avatar'    => $user['avatar'] ?? '',
               'username' => $user['username']
            ],
        );
        
        $iatrefresh = time(); // current timestamp value
        $exprefresh = $iatrefresh + getenv('JWT_REFRESH_TTL');
        $payloadRefresh = array(
            "iss" => base_url(),
            "sub" => $user['users_id'],
            "iat" => $iatrefresh, //Time the JWT issued at
            "exp" => $exprefresh, // Expiration time of token
        );
        $token = JWT::encode($payload, $key, 'HS256');
 
        $response = [
            'message' => 'Login Succesful',
            'access_token' => $token,
            'access_expired' => $payload['exp'],
            'refresh_token'=> JWT::encode($payloadRefresh, $key, 'HS256'),
            'refresh_exipred' => $payloadRefresh['exp']
        ];
         
        return $this->respond(['code' => 200, 'data' => $response], 200);
    }
}
